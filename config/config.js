const config = {
  // 启动端口
  port: 3000,
  // 数据库配置
  database: {
    DATABASE: 'pet',
    USERNAME: 'root',
    PASSWORD: '123456',
    PORT: '3306',
    HOST: 'localhost'
  },

  security: {
    secretKey: "pet",
    // 过期时间 1小时
    expiresIn: 60*60*60
  },
}

module.exports = config