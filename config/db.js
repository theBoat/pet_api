const Sequelize = require("sequelize");

const {
    DATABASE,
    USERNAME,
    PASSWORD,
    PORT,
    HOST
} = require('./config').database

const sequelize = new Sequelize(DATABASE, USERNAME, PASSWORD, {
    host: HOST, //数据库地址
    port: PORT,
    dialect: 'mysql', //指定连接的数据库类型
    pool: {
        max: 5, //连接池最大连接数量
        min: 0, //最小连接数量
        idle: 10000, //如果一个线程 10秒内么有被使用过的话，就释放
    },
    timezone: '+08:00', //for writing to database
    // logging: false, // 执行过程会log一些SQL的logging，设为false不显示
    // logging: function () {
    //     // console.log(this)
    // },
    define: {
        timestamps: true,
        timestamp: { //我们自己定义的时间戳字段
            // type: DATE,
            defaultValue: Date.now()
        },
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at',
    }
})
// 创建模型
// sequelize.sync({
//     force: false
// })

/**
 * 多表关联
 */
sequelize
    .authenticate()
    .then(async () => {
        // test
        const Note = require('../models/test/note');
        const Tag = require('../models/test/tag')
        const NoteTag = require('../models/test/noteTag');
        // 角色权限
        const Role = require('../models/role');
        const Permission = require('../models/permission');
        const RolePermission = require('../models/rolePermission');
        // 用户角色
        const Admin = require('../models/admin');
        const AdminRole = require('../models/adminRole');
        // 商家订单用户
        const shop = require('../models/shop');
        const user = require('../models/user');
        const order = require('../models/order');
        // Note的实例拥有getTags、setTags、addTag、addTags、createTag、
        // removeTag、hasTag方法
        Note.belongsToMany(Tag, {
            through: {
                model: NoteTag,
            },
            foreignKey: 'noteId',
        });

        // Tag的实例拥有getNotes、setNotes、addNote、addNotes、createNote、
        // removeNote、hasNote方法
        Tag.belongsToMany(Note, {
            through: {
                model: NoteTag,
            },
            foreignKey: 'tagId',
        });

        Role.belongsToMany(Permission, {
            through: {
                model: RolePermission,
            },
            foreignKey: 'rid',
        });
        Permission.belongsToMany(Role, {
            through: {
                model: RolePermission,
            },
            foreignKey: 'pid',
        });
        Admin.belongsToMany(Role, {
            through: {
                model: AdminRole,
            },
            foreignKey: 'aid',
        });
        Role.belongsToMany(Admin, {
            through: {
                model: AdminRole,
            },
            foreignKey: 'rid',
        });

        shop.belongsToMany(user, {
            through: {
                model: order,
            },
            foreignKey: 'shop_id',
        });
        user.belongsToMany(shop, {
            through: {
                model: order,
            },
            foreignKey: 'user_id',
        });
        // sequelize.sync({
        //         force: true
        //     })
        //     .then(async () => {

        //     })
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });
//对连接进行测试，查看控制台
/* sequelize
    .authenticate()
    .then(() => {
        console.log('******Connection has been established successfully.********');
        console.log('******测试结束，即将退出！！！********');
        process.exit(); //结束进程
    })
    .catch(err => {
        console.error('***************Unable to connect to the database:***********', err);
    }); */
module.exports = {
    sequelize
}