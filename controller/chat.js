const chatDao = require('../dao/chatDao')
const md5 = require('md5')
const jwt = require('jsonwebtoken')
const config = require('../config/config.js')

// 获取用户信箱
exports.getUserChat = async ctx => {
  let token = ctx.request.headers["authorization"];
  let payload = jwt.verify(token,config.security.secretKey)
  let {id} = payload
  console.log(id)
  let res = await chatDao.getChatByUser(id)
  ctx.body = {
    code:0,
    data:res
  }
}