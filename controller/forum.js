const forumDao = require('../dao/forumDao')
const md5 = require('md5')
const jwt = require('jsonwebtoken')
const config = require('../config/config.js')

// 发表评论
exports.publishComment = async ctx => {
  let body = ctx.request.body
  await forumDao.postComment(body)
  ctx.body = {
    code:0,
    msg:"发表成功"
  }
}

// 发表文章
exports.publish = async ctx => {
  let body = ctx.request.body
  await forumDao.publish(body)
  ctx.body = {
    code:0,
    msg:"发表成功"
  }
}

// 获取所有文章
exports.getAllPost = async ctx => {
  let res = await forumDao.getAllPost()
  ctx.body = {
    code:0,
    data:res
  }
}

// 获取某篇文章
exports.getOnePost = async ctx => {
  let {id} = ctx.request.query
  let res = await forumDao.getOnePost(id)
  ctx.body = {
    code:0,
    data:res
  }
}

// 获取文章评论
exports.getPostComment = async ctx => {
  let {id} = ctx.request.query
  console.log(id)
  let res = await forumDao.getPostComment(id)
  ctx.body = {
    code:0,
    data:res
  }
}

// 获取某篇文章
exports.getUserPost = async ctx => {
  let {id} = ctx.request.query
  let res = await forumDao.getPostByUserId(id)
  ctx.body = {
    code:0,
    data:res
  }
}