const orderDao = require('../dao/orderDao')
const chatDao = require('../dao/chatDao')
const shopDao = require('../dao/shopDao')
const jwt = require('jsonwebtoken')
const config = require('../config/config.js')

// 添加订单
exports.create = async ctx => {
  let body = ctx.request.body
  let token = ctx.request.headers["authorization"];
  let payload = jwt.verify(token, config.security.secretKey)
  let {
    id
  } = payload
  let {
    shop_id
  } = body
  body.user_id = id
  let timestamp = Date.parse(new Date());
  // 订单编号:商家id + 用户id + 事件戳
  body.number = shop_id + id + timestamp
  let res = await orderDao.create(body)
  ctx.body = {
    code: 0,
    message: '下单成功',
    data: res
  }
}

// 修改订单状态
exports.updateOrderStatus = async ctx => {
  let body = ctx.request.body
  await orderDao.updateStatus(body)
  if (body.status == 2) {
    let content = {
      user_id: body.user_id,
      shop_id: body.shop_id,
      shopname: body.shop_name,
      content: `您已支付,预约成功`
    }
    await chatDao.create(content)
  } else if (body.status == 3) {
    let content = {
      user_id: body.user_id,
      shop_id: body.shop_id,
      shopname: body.shop_name,
      content: `您的订单已完成`
    }
    await chatDao.create(content)
  }
  ctx.body = {
    code: 0,
    message: '修改订单状态成功',
  }
}
// h5获取订单
exports.getMyOrder = async ctx => {
  let {
    status
  } = ctx.request.query
  let token = ctx.request.headers["authorization"];
  let payload = jwt.verify(token, config.security.secretKey)
  let {
    id
  } = payload
  let params = {
    status: status,
    id: id
  }
  let res = await orderDao.getOrderByStatus(params)
  ctx.body = {
    code: 0,
    data: res,
  }
}

exports.getOrderDetail = async ctx => {
  let {
    id
  } = ctx.request.query
  let res = await orderDao.getOrderById(id)
  ctx.body = {
    code: 0,
    data: res,
  }
}

exports.getPetOrder = async ctx => {
  let {
    id
  } = ctx.request.query
  let res = await orderDao.getOrderByPetId(id)
  ctx.body = {
    code: 0,
    data: res,
  }
}

// 获取所有订单
exports.getAllOrder = async ctx => {
  let {
    p = 1, limit = 10
  } = ctx.request.query
  let offset = (p - 1) * limit;
  let body = {
    offset: offset,
    limit: Number(limit)
  }
  const res = await orderDao.findAll(body);
  ctx.body = {
    code: 0,
    data: res
  }
}

// pc搜索订单
exports.searchOrder = async ctx => {
  let {
    searchQuery
  } = ctx.request.query
  let res = await orderDao.searchOrder(searchQuery)
  ctx.body = {
    code: 0,
    data: res
  }
}

// 获取商家所有订单
exports.getAdminOrder = async ctx => {
  let token = ctx.request.headers["authorization"];
  let payload = jwt.verify(token, config.security.secretKey)
  let {
    shop_id
  } = payload
  if(!shop_id){
    ctx.body = {
      code: 0,
      data: []
    }
    return
  }
  let {
    p = 1, limit = 10
  } = ctx.request.query
  let offset = (p - 1) * limit;
  let body = {
    offset: offset,
    limit: Number(limit),
    shop_id: shop_id
  }
  const res = await orderDao.findAllByShopId(body);
  ctx.body = {
    code: 0,
    data: res
  }
}

exports.getShopOrder = async ctx => {

}

// 服务评价
exports.evaluation = async ctx => {
  let body = ctx.request.body
  let {
    rate: user_rate,
    quality_rate: user_quality_rate,
    attitude_rate: user_attitude_rate,
    envir_rate: user_envir_rate,
  } = body
  let {
    rate,
    quality_rate,
    attitude_rate,
    envir_rate,
    comment_count
  } = await shopDao.getOneShop(body.shop_id)
  let divisor = comment_count
  if (comment_count == 0) {
    comment_count = 1
    body.comment_count = 1
    divisor = 2
  } else{
    divisor = comment_count*1 + 1
  }
  body.rate = ((rate*comment_count + user_rate * 1) / divisor).toFixed(1)
  body.quality_rate = ((quality_rate*comment_count + user_quality_rate * 1) / divisor).toFixed(1)
  body.attitude_rate = ((attitude_rate*comment_count + user_attitude_rate * 1) / divisor).toFixed(1)
  body.envir_rate = ((envir_rate*comment_count + user_envir_rate * 1) / divisor).toFixed(1)
  console.log(body)
  await orderDao.createEvaluation(body)
  await orderDao.updateComment(body)
  if(body.comment_count !== 1) body.comment_count = comment_count * 1 + 1
  await shopDao.updateComment(body)
  ctx.body = {
    code: 0,
    msg: "评价成功",
    body: body
  }
}