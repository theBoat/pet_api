const permissionDao = require('../dao/permissionDao')
const jwt = require('jsonwebtoken')
const config = require('../config/config.js')

// 添加权限
exports.create = async ctx => {
  let body = ctx.request.body
  const res = await permissionDao.create(body);
  console.log(res)
  ctx.body = {
    code:0,
    message:'添加成功'
  }
}

// 添加权限
exports.edit = async ctx => {
  let body = ctx.request.body
  const res = await permissionDao.edit(body);
  console.log(res)
  ctx.body = {
    code:0,
    message:'修改成功'
  }
}

// 获取所有的权限
exports.getAllRules = async ctx => {
  let { p = 1,limit = 10 } = ctx.request.query
  let offset = (p - 1) * limit;
  console.log(p,limit,offset)
  let body = {
    offset:offset,
    limit:Number(limit)
  }
  const res = await permissionDao.getAllRules(body);
  ctx.body = {
    code: 0,
    data: res
  }
}