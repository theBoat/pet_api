const petDao = require('../dao/petDao')
const jwt = require('jsonwebtoken')
const config = require('../config/config.js')

// 添加宠物
exports.createPet = async ctx => {
  let body = ctx.request.body
  let token = ctx.request.headers["authorization"];
  let payload = jwt.verify(token,config.security.secretKey)
  body.user_id = payload.id
  console.log(body)
  const res = await petDao.create(body);
  ctx.body = {
    code:0,
    message:'添加成功',
  }
}

exports.deleltePet = async ctx => {
  let {id} = ctx.request.query
  const res = await petDao.delete(id);
  ctx.body = {
    code:0,
    message:'删除成功',
  }
}
// 获取某用户的所有宠物
exports.getUserPets = async ctx => {
  let token = ctx.request.headers["authorization"];
  let payload = jwt.verify(token,config.security.secretKey)
  const res = await petDao.getUserPets(payload);
  ctx.body = {
    code:0,
    data:res
  }
}

// 获取某只宠物信息
exports.getPetInfo = async ctx => {
  let query = ctx.request.query
  const res = await petDao.getPetInfo(query);
  ctx.body = {
    code:0,
    data:res
  }
}

// 修改宠物信息
exports.updatePet = async ctx => {
  let body = ctx.request.body
  const res = await petDao.update(body);
  ctx.body = {
    code:0,
    message:'修改成功',
  }
}