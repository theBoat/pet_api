const shopDao = require('../dao/shopDao')
const shopServiceDao = require('../dao/shopServiceDao')
const adminDao = require('../dao/adminDao')
const md5 = require('md5')
const jwt = require('jsonwebtoken')
const config = require('../config/config.js')

exports.update = async ctx => {
  // 判断是否已创建商家  有则更新 无则创建
  let body = ctx.request.body
  let token = ctx.request.headers["authorization"];
  let payload = jwt.verify(token,config.security.secretKey)
  body.uid = payload.id
  let { id } = payload
  let { shop_id } = await adminDao.findById(id)
  // 创建商家
  if(!shop_id){
    let res = await shopDao.create(body)
    let params = {
      id:payload.id,
      shop_id:res.id
    }
    await adminDao.updateAdminShopId(params)
    ctx.body = {
      code:0,
      msg:'创建成功',
      shop_id:res.id
    }
  } else{
    body.id = shop_id
    await shopDao.update(body)
    ctx.body = {
      code:0,
      msg:'更新成功'
    }
  }
}

// 获取某商家信息
exports.getAllShop = async ctx => {
  let { p = 1,limit = 10 } = ctx.request.query
  let offset = (p - 1) * limit;
  let body = {
    offset:offset,
    limit:Number(limit)
  }
  const res = await shopDao.findAll(body);
  ctx.body = {
    code: 0,
    data: res
  }
}

exports.getOneShop = async ctx => {
  let token = ctx.request.headers["authorization"];
  let payload = jwt.verify(token,config.security.secretKey)
  let { id } = payload
  let { shop_id } = await adminDao.findById(id)
  let res = await shopDao.getOneShop(shop_id)
  ctx.body = {
    code:0,
    data:res
  }
}

// h5获取商家详细信息
exports.getShopInfo = async ctx => {
  let {id} = ctx.request.query
  let {dataValues:res} = await shopDao.getShopById(id)
  let fooster_service = await shopServiceDao.getAllFooster(id)
  res.fooster_service = fooster_service
  ctx.body = {
    code:0,
    data:res
  }
}

// pc获取商家寄养服务
exports.getService = async ctx => {
  let token = ctx.request.headers["authorization"];
  let payload = jwt.verify(token,config.security.secretKey)
  let { id } = payload
  let { shop_id } = await adminDao.findById(id)
  let res = await shopServiceDao.getAllFooster(shop_id)
  ctx.body = {
    code:0,
    data:res
  }
}

// h5获取商家寄养服务
exports.getServiceById = async ctx => {
  let {shop_id} = ctx.request.query
  let res = await shopServiceDao.getAllFooster(shop_id)
  ctx.body = {
    code:0,
    data:res
  }
}

// 商家创建寄养服务
exports.createService = async ctx => {
  let body = ctx.request.body
  let token = ctx.request.headers["authorization"];
  let payload = jwt.verify(token,config.security.secretKey)
  let { id,shop_id } = payload
  body.type = 1
  if(shop_id){body.shop_id = shop_id}
  let res = await shopServiceDao.create(body)
  ctx.body = {
    code:0,
    msg:'创建成功'
  }
}

// 修改商家寄养服务
exports.updateService = async ctx => {
  let body = ctx.request.body
  await shopServiceDao.update(body)
  ctx.body = {
    code:0,
    msg:'修改成功'
  }
}

// 修改商家寄养服务
exports.delService = async ctx => {
  let body = ctx.request.body
  await shopServiceDao.delete(body)
  ctx.body = {
    code:0,
    msg:'删除成功'
  }
}

// 获取附近商家 (暂且根据city id查找)
exports.getNearbyShop = async ctx => {
  let {city} = ctx.request.query
  let res = await shopDao.getNearbyShop(city)
  ctx.body = {
    code:0,
    data:res
  }
}

// h5首页服务查询  暂且根据city查询
exports.searchService = async ctx => {
  let {city,searchType,type,chooseType} = ctx.request.query
  let res = await shopDao.getNearbyShop(city,chooseType)
  
  ctx.body = {
    code:0,
    data:res
  }
}

// h5搜索商家  根据商家名查询
exports.searchShop = async ctx => {
  let {shopname} = ctx.request.query
  let res = await shopDao.getShopByName(shopname)
  ctx.body = {
    code:0,
    data:res
  }
}

// 获取某商家所有的用户
exports.getShopUser = async ctx => {
  let token = ctx.request.headers["authorization"];
  let payload = jwt.verify(token,config.security.secretKey)
  let { shop_id } = payload
  let body = {

  }
  let res = await shopDao.getShopUser(body)
  // const shop = await shopDao.getOneShop(shop_id)
  // const user = await Note.findAll({where: {id:[1]}})
  ctx.body = {
    code:0,
    data:res
  }  
}

// 获取商家评论
exports.getShopComment = async ctx => {
  let {shop_id} = ctx.request.query
  let res = await shopDao.getEvaluationByShopId(shop_id)
  ctx.body = {
    code:0,
    data:res
  } 
}