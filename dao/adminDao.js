const admin = require('../models/admin')

class adminDao {
  static async test(v) {
    const res = await admin.create(
      v
    );
    return res
  }
  // 获取单个用户信息
  static async findByName(v) {
    const res = await admin.findOne({
      where: {
        username: v.username
      }
    });
    return res
  }
  // 获取单个用户信息
  static async findById(id) {
    const res = await admin.findOne({
      where: {
        id: id
      }
    });
    return res
  }
  // 创建账号
  static async create(v) {
    const res = await admin.create({
      username: v.username,
      password: v.password,
      above_id: v.above_id
    });
    return res
  }
  // 获取所有账号
  static async getAllAdmin(v) {
    const res = await admin.findAndCountAll({
      limit: v.limit,
      offset: v.offset
    })
    let result = {}
    result.data = res.rows;
    result.total = res.count;
    return result
  }
  // 修改账号信息
  static async updateAdminShopId(v) {
    const res = await admin.update(
      {shop_id : v.shop_id}
    ,{
      where:{id:v.id}
    });
    return res
  }
}

module.exports = adminDao