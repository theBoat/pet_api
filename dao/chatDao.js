const chat = require('../models/chat')

class chatDao {
  // 创建一条消息
  static async create(v) {
    const res = await chat.create(
      v
    );
    return res
  }
  // 创建一条消息
  static async getChatByUser(v) {
    const res = await chat.findAll({
      where: {
        user_id: v
      },
      order:[
        ['id','DESC']
      ]
    });
    return res
  }
}

module.exports = chatDao