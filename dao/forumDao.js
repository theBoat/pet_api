const publish = require('../models/publish')
const publishComment = require('../models/publish_comment')

class forumDao {
  // 发表文章
  static async publish(v) {
    const res = await publish.create(
      v
    );
    return res
  }
  // 发表评论
  static async postComment(v) {
    const res = await publishComment.create(
      v
    );
    return res
  }
  // 获取所有文章
  static async getAllPost(v) {
    const res = await publish.findAll({
      order: [
        ['id', 'DESC']
      ]
    });
    return res
  }
  // 获取某篇文章
  static async getOnePost(id) {
    const res = await publish.findOne({
      where: {
        id: id
      }
    });
    return res
  }
  // 获取某篇文章
  static async getPostByUserId(id) {
    const res = await publish.findAll({
      where: {
        user_id: id
      }
    });
    return res
  }
  // 获取某文章所有评论
  static async getPostComment(id) {
    const res = await publishComment.findAll({
      where: {
        publish_id: id
      },
      order:[
        ['id', 'DESC']
      ]
    }, );
    return res
  }
}

module.exports = forumDao