const order = require('../models/order')
const evaluation = require('../models/shop_evaluation')
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class orderDao {
  // 创建订单
  static async create(v) {
    const res = await order.create(
      v
    );
    return res
  }
  // 更新订单状态
  static async updateStatus(v) {
    const res = await order.update({
      status: v.status
    }, {
      where: {
        id: v.id
      }
    });
    return res
  }
  // 获取订单详情
  static async getOrderById(id) {
    const res = await order.findOne({
      where: {
        id: id
      }
    });
    return res
  }
  // 获取宠物订单
  static async getOrderByPetId(id) {
    const res = await order.findAll({
      where: {
        pet_id: id
      },
      order: [
        ['id', 'DESC']
      ]
    });
    return res
  }
  // h5获取订单
  static async getOrderByStatus(v) {
    let res
    if (v.status == 0) {
      res = await order.findAll({
        where: {
          user_id: v.id
        },
        order: [
          ['id', 'DESC']
        ]
      });
      return res
    }
    res = await order.findAll({
      where: {
        user_id: v.id,
        status: v.status
      },
      order: [
        ['id', 'DESC']
      ]
    });
    return res
  }
  // 获取所有订单
  static async findAll(v) {
    const res = await order.findAndCountAll({
      limit: v.limit,
      offset: v.offset,
      order: [
        ['id', 'DESC']
      ]
    })
    let result = {}
    result.data = res.rows;
    result.total = res.count;
    return result
  }
  // 获取商家所有订单
  static async findAllByShopId(v) {
    const res = await order.findAndCountAll({
      limit: v.limit,
      offset: v.offset,
      order: [
        ['id', 'DESC']
      ]
    }, {
      where: {
        shop_id: v.shop_id
      },
      // order: [
      //   ['id', 'DESC']
      // ]
    })
    let result = {}
    result.data = res.rows;
    result.total = res.count;
    return result
  }
  // 搜索用户 根据用户名 真实姓名查询(单参数多字段查询)
  static async searchOrder(v) {
    const res = await order.findAll({
      where: {
        [Op.or]: [{
          shop_name: {
            [Op.like]: '%' + v + '%'
          }
        }, {
          number: {
            [Op.like]: '%' + v + '%'
          }
        }]
      }
    });
    return res
  }
  // 用户评价
  static async createEvaluation(v) {
    const res = await evaluation.create(
      v
    );
    return res
  }
  // 更新订单：已评价
  static async updateComment(v) {
    const res = await order.update({
      isComment: '1'
    }, {
      where: {
        id: v.order_id
      }
    });
    console.log(res)
    return res
  }
}

module.exports = orderDao