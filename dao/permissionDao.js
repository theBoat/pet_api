const permission= require('../models/permission')

class permissionDao {
  // 添加权限
  static async create(v) {
    const res = await permission.create({
      title:v.title,
      number:v.number,
      description:v.description,
      status:1
    })
    return res
  }
  // 修改权限
  static async edit(v) {
    const res = await permission.update(v,{
      where:{id:v.id}
    })
    return res
  }
  // 获取所有权限
  static async getAllRules(v) {
    const res = await permission.findAndCountAll({
      limit: v.limit,
      offset: v.offset 
    })
    let result = {}
    result.data = res.rows;
    result.total = res.count;
    return result
  }
}

module.exports = permissionDao