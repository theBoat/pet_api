const pet= require('../models/pet')

class petDao {
  // 添加宠物
  static async create(v) {
    const res = await pet.create(v)
    return res
  }

  // 删除宠物
  static async delete(id) {
    const res = await pet.destroy({
      where:{
        id:id
      }
    })
    return res
  }

  // 修改宠物信息
  static async update(v) {
    const res = await pet.update(v, {
      where: {
        id: v.id
      }
    })
    return res
  }

  // 获取用户所有宠物信息
  static async getUserPets(v) {
    const res = await pet.findAll({
      where: {
        user_id: v.id
      }
    });
    return res
  }

  // 获取某只宠物信息
  static async getPetInfo(v) {
    const res = await pet.findOne({
      where: {
        id: v.id
      }
    });
    return res.dataValues
  }
}

module.exports = petDao