const shop = require('../models/shop')
const user = require('../models/user')
const evaluation = require('../models/shop_evaluation')
const order = require('../models/order')
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class shopDao {
  // 创建商店
  static async create(v) {
    const res = await shop.create(
      v
    );
    return res
  }
  // 更新
  static async update(v) {
    const res = await shop.update(
      v, {
        where: {
          id: v.id
        }
      }
    );
    return res
  }
  // 获取某商家信息
  static async getOneShop(id) {
    const res = await shop.findOne({
      where: {
        id: id
      }
    });
    return res
  }
  // 获取所有商家信息
  static async findAll() {
    const res = await shop.findAll();
    return res
  }
  // 获取某商家信息
  static async getShopById(id) {
    const res = await shop.findOne({
      where: {
        id: id
      }
    });
    return res
  }
  // 获取某城市信息(根据city)
  static async getNearbyShop(id,chooseType) {
    if(chooseType == 1){
      const res = await shop.findAll({
        where: {
          city: id
        },
        order: [
          ['rate', 'DESC']
        ]
      });
      return res
    }
    const res = await shop.findAll({
      where: {
        city: id
      }
    });
    return res
  }
  // 搜索商家 根据商家名查询
  static async getShopByName(shopname) {
    const res = await shop.findAll({
      where: {
        shopname: {
          [Op.like]: '%' + shopname + '%'
        }
      }
    });
    return res
  }
  // 获取所有商家
  static async findAll(v) {
    const res = await shop.findAndCountAll({
      limit: v.limit,
      offset: v.offset
    })
    let result = {}
    result.data = res.rows;
    result.total = res.count;
    return result
  }
  // test 

  // 获取商家所有客户
  static async getShopUser(v) {
    // const tags = await Tag.findOne({where: {id:1}})
    // const notes = await Note.findAll({where: {id:[1]}})
    // let res = await tags.setNotes(notes)

    const shopInfo = await shop.findAll({
      where: {
        id: 1
      }
    })
    const userInfo = await user.findAll()
    let res = await shopInfo.setUsers(userInfo)

    // let res = await shop.findAll({
    //   include: [{
    //     model: user,
    //     attributes: ['id']
    //   }]
    // })
    console.log(res)
    return res
  }
  // 更新商家评论信息
  static async updateComment(v) {
    const res = await shop.update({
      rate: v.rate,
      quality_rate: v.quality_rate,
      attitude_rate: v.attitude_rate,
      envir_rate: v.envir_rate,
      comment_count: v.comment_count
    }, {
      where: {
        id: v.shop_id
      }
    });
    return res
  }
  // 获取某商家的评论
  static async getEvaluationByShopId(id) {
    const res = await evaluation.findAll({
      where: {
        shop_id: id
      },
      order: [
        ['id', 'DESC']
      ]
    });
    return res
  }
}

module.exports = shopDao