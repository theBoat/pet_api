const shopService = require('../models/shop_service')

class shopServiceDao {
  // 创建商店寄养服务
  static async create(v) {
    const res = await shopService.create(
      v
    );
    return res
  }
  // 获取商店所有寄养服务
  static async getAllFooster(v) {
    const res = await shopService.findAll({
      where: {
        shop_id: v
      }
    });
    return res
  }
  // 修改商店寄养服务
  static async update(v) {
    const res = await shopService.update(
      v, {
        where: {
          id: v.id
        }
      }
    );
    return res
  }
  // 删除商店寄养服务
  static async delete(v) {
    const res = await shopService.destroy({
      where: {
        id: v.id
      }
    });
    return res
  }
}

module.exports = shopServiceDao