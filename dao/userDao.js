const user = require('../models/user')
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class userDao {
  // 新增用户
  static async create(v) {
    const res = await user.create({
      username: v.username,
      password: v.password
    })
    return res
  }

  // 更新用户信息
  static async update(v) {
    const res = await user.update(v, {
      where: {
        id: v.id
      }
    })
    return res
  }

  // 获取单个用户信息
  static async findById(v) {
    const res = await user.findOne({
      where: {
        id: v.id
      }
    });
    return res.dataValues
  }

  // 获取所有账号
  static async findAll(v) {
    const res = await user.findAndCountAll({
      limit: v.limit,
      offset: v.offset
    })
    let result = {}
    result.data = res.rows;
    result.total = res.count;
    return result
  }

  // 搜索用户 根据用户名 真实姓名查询(单参数多字段查询)
  static async getUserByName(v) {
    const res = await user.findAll({
      where: {
        [Op.or]: [{
          nickname: {
            [Op.like]: '%' + v + '%'
          }
        }, {
          truename: {
            [Op.like]: '%' + v + '%'
          }
        }, {
          phone: {
            [Op.like]: '%' + v + '%'
          }
        }]
      }
    });
    return res
  }

  // 判断是否已注册
  static async findByName(v) {
    const res = await user.findOne({
      where: {
        username: v.username
      }
    });
    return res
  }

  // 判断是否已注册
  static async findByName(v) {
    const res = await user.findOne({
      where: {
        username: v.username
      }
    });
    return res
  }
}


module.exports = userDao