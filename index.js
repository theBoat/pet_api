const koa = require('koa')
const routers = require('./routes/index')
const bodyParser = require('koa-body');
const cors = require('koa2-cors');
const koaStatic = require('koa-static')

require('./libs/global_function')

const config = require('./config/config.js')

const app = new koa()

// 中间件
// app.use(bodyParser())
app.use(bodyParser({
  enableTypes: ['json', 'form', 'text'],
  multipart: true // 是否支持 multipart-formdate 的表单
}))

// 解决跨域   初始化路由中间件
app.use(cors());
app.use(routers.routes()).use(routers.allowedMethods())

// 静态资源
app.use(koaStatic(__dirname+'/public'))

app.listen(config.port)

console.log(`running...`)