let glb = {
  success(data) {
    this.ctx.body = {
      code: 0,
      message: 'success',
      data: data,
    }
  },
  error(message = 'error', code = 1, data = null) {
    console.log(message, code)
    this.ctx.body = {
      code: code,
      message: message,
      data: data,
    }
    return
  },
  async get(ctx){
    console.log('ctx',this.ctx)
  }
}

Object.assign(global, glb);