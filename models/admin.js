const {
  sequelize
} = require('../config/db');
const {
  Sequelize,
  Model
} = require('sequelize')

const admin = sequelize.define("admin", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  username:{
    type: Sequelize.STRING(16),
    allowNull: false,
    comment: '用户名'
  },
  password:{
    type: Sequelize.STRING(64),
    allowNull: false,
    comment: '密码'
  },
  role:{
    type: Sequelize.STRING(64),
    allowNull: true,
    comment: '角色'
  },
  shop_id:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '商家id'
  },
  above_id:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '上级id'
  },
  below_id:{
    type: Sequelize.STRING(11),
    allowNull: true,
    comment: '下级id'
  }
});
(async function () {
  await admin.sync({
      alter: true //这将检查数据库中表的当前状态(它具有哪些列,它们的数据类型等),然后在表中进行必要的更改以使其与模型匹配.
  });
})();

module.exports = admin