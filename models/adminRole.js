const {
  sequelize
} = require('../config/db');
const {
  Sequelize,
  Model
} = require('sequelize')

const adminRole = sequelize.define("admin_Role", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  aid:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '管理员id'
  },
  rid:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '角色id'
  }
});
(async function () {
  await adminRole.sync({
      alter: true //这将检查数据库中表的当前状态(它具有哪些列,它们的数据类型等),然后在表中进行必要的更改以使其与模型匹配.
  });
})();


module.exports = adminRole