const {
  Sequelize,
  Model
} = require('sequelize')
const {
  sequelize
} = require('../config/db');

class chat extends Model {

}

chat.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  user_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '用户id'
  },
  shop_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '商家id'
  },
  shopname:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '商家名'
  },
  content:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '内容'
  }
}, {
  sequelize,
  modelName: 'chat',
  tableName: 'chat'
})

// chat.sync({
//   force: true
// })

module.exports = chat