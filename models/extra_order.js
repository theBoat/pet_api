const {
  Sequelize,
  Model
} = require('sequelize')
const {
  sequelize
} = require('../config/db');

// 定义模型
class extra_order extends Model {

}

// 初始模型
extra_order.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  order_id:{
    type: Sequelize.INT(11),
    allowNull: false,
    comment: '订单id'
  },
  user_id:{
    type: Sequelize.INT(11),
    allowNull: false,
    comment: '用户id'
  },
  pet_id:{
    type: Sequelize.INT(11),
    allowNull: false,
    comment: '宠物id'
  },
  shop_id:{
    type: Sequelize.INT(11),
    allowNull: false,
    comment: '商家id'
  },
  service_id:{
    type: Sequelize.INT(11),
    allowNull: false,
    comment: '服务id'
  }
}, {
  sequelize,
  modelName: 'extra_order',
  tableName: 'extra_order'
})

module.exports = extra_order