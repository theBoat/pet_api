const {
  sequelize
} = require('../config/db');
const {
  Sequelize,
  Model
} = require('sequelize')

const order = sequelize.define("order", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  shop_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '商家id'
  },
  shop_name:{
    type: Sequelize.STRING(32),
    allowNull: false,
    comment: '商家名称'
  },
  user_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '用户id'
  },
  pet_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '宠物id'
  },
  pet_image:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '宠物图片'
  },
  service_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '服务id'
  },
  service_name:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '服务名称'
  },
  service_image:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '服务照片'
  },
  start_day:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '开始日期'
  },
  end_day:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '结束日期'
  },
  total_day:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '宠物id'
  },
  booking_time:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '预约时间'
  },
  total_price:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '总费用'
  },
  message:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '客户备注'
  },
  number:{
    type: Sequelize.STRING(32),
    allowNull: false,
    comment: '订单编号'
  },
  isComment:{
    type: Sequelize.INTEGER(32),
    allowNull: false,
    comment: '是否评论'
  },
  status:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '订单状态'
  }
});
(async function () {
  await order.sync({
      alter: true //这将检查数据库中表的当前状态(它具有哪些列,它们的数据类型等),然后在表中进行必要的更改以使其与模型匹配.
  });
})();


module.exports = order


// const {
//   Sequelize,
//   Model
// } = require('sequelize')
// const {
//   sequelize
// } = require('../config/db');

// class order extends Model {

// }

// order.init({
//   id: {
//     type: Sequelize.INTEGER,
//     primaryKey: true,
//     autoIncrement: true
//   },
//   shop_id:{
//     type: Sequelize.INTEGER(11),
//     allowNull: false,
//     comment: '商家id'
//   },
//   shop_name:{
//     type: Sequelize.STRING(32),
//     allowNull: false,
//     comment: '商家名称'
//   },
//   user_id:{
//     type: Sequelize.INTEGER(11),
//     allowNull: false,
//     comment: '用户id'
//   },
//   pet_id:{
//     type: Sequelize.INTEGER(11),
//     allowNull: false,
//     comment: '宠物id'
//   },
//   pet_image:{
//     type: Sequelize.STRING,
//     allowNull: false,
//     comment: '宠物图片'
//   },
//   service_id:{
//     type: Sequelize.INTEGER(11),
//     allowNull: false,
//     comment: '服务id'
//   },
//   service_name:{
//     type: Sequelize.STRING,
//     allowNull: false,
//     comment: '服务名称'
//   },
//   service_image:{
//     type: Sequelize.STRING,
//     allowNull: false,
//     comment: '服务照片'
//   },
//   start_day:{
//     type: Sequelize.STRING,
//     allowNull: false,
//     comment: '开始日期'
//   },
//   end_day:{
//     type: Sequelize.STRING,
//     allowNull: false,
//     comment: '结束日期'
//   },
//   total_day:{
//     type: Sequelize.INTEGER(11),
//     allowNull: false,
//     comment: '宠物id'
//   },
//   booking_time:{
//     type: Sequelize.STRING,
//     allowNull: false,
//     comment: '预约时间'
//   },
//   total_price:{
//     type: Sequelize.INTEGER(11),
//     allowNull: false,
//     comment: '总费用'
//   },
//   message:{
//     type: Sequelize.STRING,
//     allowNull: false,
//     comment: '客户备注'
//   },
//   number:{
//     type: Sequelize.STRING(32),
//     allowNull: false,
//     comment: '订单编号'
//   },
//   status:{
//     type: Sequelize.INTEGER(11),
//     allowNull: false,
//     comment: '订单状态'
//   }
// }, {
//   sequelize,
//   modelName: 'order',
//   tableName: 'order'
// })

// // order.sync({
// //   force: true
// // })

// module.exports = order