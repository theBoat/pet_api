const {
  sequelize
} = require('../config/db');
const {
  Sequelize,
  Model
} = require('sequelize')

const permission = sequelize.define("permission", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  title:{
    type: Sequelize.STRING(11),
    allowNull: false,
    comment: '权限名称'
  },
  number:{
    type: Sequelize.STRING(11),
    allowNull: false,
    comment: '权限编号'
  },
  description:{
    type: Sequelize.STRING(11),
    allowNull: true,
    comment: '权限描述'
  },
  status:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '状态'
  },
});

(async function () {
  await permission.sync({
      alter: true //这将检查数据库中表的当前状态(它具有哪些列,它们的数据类型等),然后在表中进行必要的更改以使其与模型匹配.
  });
})();

// permission.sync({
//   force: true
// })

module.exports = permission