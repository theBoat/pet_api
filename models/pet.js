const {
  Sequelize,
  Model
} = require('sequelize')
const {
  sequelize
} = require('../config/db');

class pet extends Model {

}

pet.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  type:{
    type: Sequelize.INTEGER(1),
    allowNull: false,
    comment: '宠物类型'
  },
  user_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '主人id'
  },
  avatar:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '头像'
  },
  nickname:{
    type: Sequelize.STRING(11),
    allowNull: false,
    comment: '昵称'
  },
  age:{
    type: Sequelize.STRING(11),
    allowNull: true,
    comment: '年龄',
  },
  gender:{
    type: Sequelize.INTEGER(1),
    allowNull: true,
    comment: '性别'
  },
  kind:{
    type: Sequelize.STRING(11),
    allowNull: true,
    comment: '品种'
  },
  weight:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '重量'
  },
  certificate:{
    type: Sequelize.STRING,
    allowNull: true,
    comment: '宠物证件'
  },
  // vaccine_image:{
  //   type: Sequelize.STRING,
  //   allowNull: false,
  //   comment: '证'
  // },
  // vaccine_situation:{
  //   type: Sequelize.INT(11),
  //   allowNull: false,
  //   comment: '情况'
  // },
  state:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '寄养状态'
  },
}, {
  sequelize,
  modelName: 'pet',
  tableName: 'pet'
})

// pet.sync({
//   force: true
// })

module.exports = pet