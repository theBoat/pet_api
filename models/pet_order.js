const {
  Sequelize,
  Model
} = require('sequelize')
const {
  sequelize
} = require('../config/db');

class order extends Model {

}

order.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  pet_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '宠物id'
  },
  service_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '服务id'
  },
  number:{
    type: Sequelize.INTEGER(17),
    allowNull: false,
    comment: '订单编号'
  },
  status:{
    type: Sequelize.INTEGER(1),
    allowNull: false,
    comment: '状态'
  }
}, {
  sequelize,
  modelName: 'order',
  tableName: 'order'
})


// order.sync({
//   force: true
// })

module.exports = order