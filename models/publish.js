const {
  Sequelize,
  Model
} = require('sequelize')
const {
  sequelize
} = require('../config/db');

class publish extends Model {

}

publish.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  user_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '用户id'
  },
  nickname:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '用户名称'
  },
  avatar:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '用户头像'
  },
  title:{
    type: Sequelize.STRING,
    allowNull: true,
    comment: '标题'
  },
  content:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '内容'
  },
}, {
  sequelize,
  modelName: 'publish',
  tableName: 'publish'
})

// publish.sync({
//   force: true
// })

module.exports = publish