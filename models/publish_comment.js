const {
  Sequelize,
  Model
} = require('sequelize')
const {
  sequelize
} = require('../config/db');

class publishComment extends Model {

}

publishComment.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  publish_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '文章id'
  },
  user_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '用户id'
  },
  nickname:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '用户名称'
  },
  avatar:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '用户头像'
  },
  content:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '内容'
  },
}, {
  sequelize,
  modelName: 'publish_comment',
  tableName: 'publish_comment'
})

// publishComment.sync({
//   force: true
// })

module.exports = publishComment