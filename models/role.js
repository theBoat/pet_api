const {
  sequelize
} = require('../config/db');
const {
  Sequelize,
  Model
} = require('sequelize')

const role = sequelize.define("role", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING(11),
    allowNull: false,
    comment: '角色名称'
  },
  description: {
    type: Sequelize.STRING(32),
    allowNull: false,
    comment: '说明'
  },
  status: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '状态'
  }
});
(async function () {
  await role.sync({
      alter: true //这将检查数据库中表的当前状态(它具有哪些列,它们的数据类型等),然后在表中进行必要的更改以使其与模型匹配.
  });
})();


// role.sync({
//   force: true
// })

module.exports = role