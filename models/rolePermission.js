const {
  sequelize
} = require('../config/db');
const {
  Sequelize,
  Model
} = require('sequelize')


const rolePermission = sequelize.define("role_Permission", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  rid:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '角色id'
  },
  pid:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '权限id'
  }
});

(async function () {
  await rolePermission.sync({
      alter: true //这将检查数据库中表的当前状态(它具有哪些列,它们的数据类型等),然后在表中进行必要的更改以使其与模型匹配.
  });
})();


// rolePermission.sync({
//   force: true
// })

module.exports = rolePermission