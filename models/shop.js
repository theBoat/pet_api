const {
  Sequelize,
  Model
} = require('sequelize')
const {
  sequelize
} = require('../config/db');

// 定义模型
class shop extends Model {

}

// 初始模型
shop.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  uid:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '管理员id'
  },
  shopname:{
    type: Sequelize.STRING(32),
    allowNull: false,
    comment: '商店名'
  },
  shop_image:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '商店头像'
  },
  banner:{
    type: Sequelize.STRING,
    allowNull: true,
    comment: '轮播图'
  },
  city:{
    type: Sequelize.STRING(16),
    allowNull: false,
    comment: '城市'
  },
  address:{
    type: Sequelize.STRING(64),
    allowNull: false,
    comment: '地址'
  },
  rate:{
    type: Sequelize.STRING(11),
    allowNull: true,
    comment: '综合评分',
    defaultValue:0
  },
  quality_rate:{
    type: Sequelize.STRING(11),
    allowNull: true,
    comment: '服务质量',
    defaultValue:5
  },
  attitude_rate:{
    type: Sequelize.STRING(11),
    allowNull: true,
    comment: '服务态度',
    defaultValue:5
  },
  envir_rate:{
    type: Sequelize.STRING(11),
    allowNull: true,
    comment: '服务环境',
    defaultValue:5
  },
  start_day:{
    type: Sequelize.STRING(32),
    allowNull: true,
    comment: '营业开始时间(天)'
  },
  end_day:{
    type: Sequelize.STRING(32),
    allowNull: true,
    comment: '营业结束时间(天)'
  },
  start_time:{
    type: Sequelize.STRING(32),
    allowNull: true,
    comment: '营业开始时间(时)'
  },
  end_time:{
    type: Sequelize.STRING(32),
    allowNull: true,
    comment: '营业结束时间(时)'
  },
  big_cage:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '大型犬笼子'
  },
  middle_cage:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '中型犬笼子'
  },
  small_cage:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '小型犬笼子'
  },
  cat_cage:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '小型犬笼子'
  },
  rm_big_cage:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '剩余大型犬笼子'
  },
  rm_middle_cage:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '剩余中型犬笼子'
  },
  rm_small_cage:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '剩余小型犬笼子'
  },
  rm_cat_cage:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '剩余小型犬笼子'
  },
  comment_count:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '评论条数',
    defaultValue:0
  },
  medical_service:{
    type: Sequelize.STRING(999),
    allowNull: true,
    comment: '医护服务'
  },
  other_service:{
    type: Sequelize.STRING,
    allowNull: true,
    comment: '其他服务'
  },
  environment:{
    type: Sequelize.STRING(999),
    allowNull: true,
    comment: '寄养环境'
  },
  description:{
    type: Sequelize.STRING,
    allowNull: true,
    comment: '商店描述'
  },
  posters_list:{
    type: Sequelize.STRING,
    allowNull: true,
    comment: '商家宣传图'
  },
  notice:{
    type: Sequelize.STRING,
    allowNull: true,
    comment: '寄养须知'
  },
  // phone:{
  //   type: Sequelize.STRING,
  //   allowNull: true,
  //   comment: '联系方式'
  // },
}, {
  sequelize,
  modelName: 'shop',
  tableName: 'shop'
})

// shop.sync({
//   force: true
// })

module.exports = shop