const {
  Sequelize,
  Model
} = require('sequelize')
const {
  sequelize
} = require('../config/db');

class shop_evaluation extends Model {

}

shop_evaluation.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  shop_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '商家id'
  },
  user_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '用户id'
  },
  order_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '订单id'
  },
  nickname:{
    type: Sequelize.STRING(30),
    allowNull: false,
    comment: '用户昵称'
  },
  avatar:{
    type: Sequelize.STRING,
    allowNull: false,
    comment: '用户头像'
  },
  quality_rate:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '服务质量',
    defaultValue:5
  },
  attitude_rate:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '服务态度',
    defaultValue:5
  },
  envir_rate:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '服务环境',
    defaultValue:5
  },
  content:{
    type: Sequelize.STRING,
    allowNull: true,
    comment: '评论内容'
  },
}, {
  sequelize,
  modelName: 'shop_evaluation',
  tableName: 'shop_evaluation'
})

// shop_evaluation.sync({
//   force: true
// })

module.exports = shop_evaluation