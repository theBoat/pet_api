const {
  Sequelize,
  Model
} = require('sequelize')
const {
  sequelize
} = require('../config/db');

class shop_service extends Model {

}

shop_service.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  shop_id:{
    type: Sequelize.INTEGER(11),
    allowNull: false,
    comment: '商店id'
  },
  type:{
    type: Sequelize.STRING(11),
    allowNull: false,
    comment: '服务类型'
  },
  service_name:{
    type: Sequelize.STRING(11),
    allowNull: false,
    comment: '服务名称'
  },
  cage_id:{
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '笼子类别'
  },
  image:{
    type: Sequelize.STRING,
    allowNull: true,
    comment: '图标'
  },
  price:{
    type: Sequelize.STRING(11),
    allowNull: false,
    comment: '价格'
  },
}, {
  sequelize,
  modelName: 'shop_service',
  tableName: 'shop_service'
})

// shop_service.sync({
//   force: false
// })

module.exports = shop_service