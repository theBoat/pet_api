const {
    sequelize
} = require('../../config/db');
const {
    Sequelize,
    Model
} = require('sequelize')

const NoteTag = sequelize.define("t_Note_Tag", {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    noteId: {
        type: Sequelize.INTEGER,
        allowNull:false,
    },
    tagId:{
        type: Sequelize.INTEGER,
        allowNull:false,
    },
});

(async function () {
    await NoteTag.sync({
        alter: true //这将检查数据库中表的当前状态(它具有哪些列,它们的数据类型等),然后在表中进行必要的更改以使其与模型匹配.
    });
})();
module.exports = NoteTag;
// module.exports = sequelize => {
//     const NoteTag = sequelize.define("NoteTag", {
        // id: {
        //     type: Sequelize.INTEGER,
        //     primaryKey: true,
        //     autoIncrement: true
        // },

        // noteId: {
        //     type: Sequelize.INTEGER,
        //     allowNull:false,
        // },
        // tagId:{
        //     type: Sequelize.INTEGER,
        //     allowNull:false,
        // },
//     });

//     return NoteTag;
// };