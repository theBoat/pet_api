const {
    sequelize
} = require('../../config/db');
const {
    Sequelize,
    Model
} = require('sequelize')
const note = sequelize.define("t_note", {
    title: {
        type: Sequelize.CHAR(64),
        allowNull: false
    }
});
(async function () {
    await note.sync({
        alter: true //这将检查数据库中表的当前状态(它具有哪些列,它们的数据类型等),然后在表中进行必要的更改以使其与模型匹配.
    });
})();
module.exports = note;


// const Sequelize = require("sequelize");

// module.exports = sequelize => {
//     const Note = sequelize.define("note", {
        // title: {
        //     type: Sequelize.CHAR(64),
        //     allowNull: false
        // }
//     });

//     return Note;
// };