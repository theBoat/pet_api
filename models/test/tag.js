// const Sequelize = require("sequelize");

// module.exports = sequelize => {
//     const Tag = sequelize.define("tag", {
// name: {
//     type: Sequelize.CHAR(64),
//     allowNull: false,
//     unique: true
// }
//     });

//     return Tag;
// };
const {
    sequelize
} = require('../../config/db');
const {
    Sequelize,
    Model
} = require('sequelize')
const tag = sequelize.define("t_tag", {
    name: {
        type: Sequelize.CHAR(64),
        allowNull: false,
    }
});
(async function () {
    await tag.sync({
        alter: true //这将检查数据库中表的当前状态(它具有哪些列,它们的数据类型等),然后在表中进行必要的更改以使其与模型匹配.
    });
})();
module.exports = tag;