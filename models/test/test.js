// const Sequelize = require("sequelize");
const {
  Sequelize,
  Model
} = require('sequelize')
const {
  sequelize
} = require('../../config/db');


// 定义模型
class test extends Model {

}

// 初始模型
test.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  title: {
    type: Sequelize.STRING(64),
    allowNull: false,
    comment: '测试'
  }
}, {
  sequelize,
  modelName: 't_test',
  tableName: 't_test'
})

// test.sync({
//   force: true
// })

module.exports = test


// const {
//   sequelize
// } = require('../config/db');
// const {
//   Sequelize,
//   Model
// } = require('sequelize')
// const test = sequelize.define("test", {
//   id: {
//     type: Sequelize.INTEGER,
//     primaryKey: true,
//     autoIncrement: true
//   },
//   title: {
//     type: Sequelize.STRING(64),
//     allowNull: false,
//     comment: '测试'
//   }
// });
// (async function () {
//   await test.sync({
//     alter: true //这将检查数据库中表的当前状态(它具有哪些列,它们的数据类型等),然后在表中进行必要的更改以使其与模型匹配.
//   });
//   console.log("模型Admin同步完毕！");
// })();
// module.exports = test;