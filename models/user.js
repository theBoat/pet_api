const {
  Sequelize,
  Model
} = require('sequelize')
const {
  sequelize
} = require('../config/db');

// 定义模型
class user extends Model {

}

// 初始模型
user.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  username: {
    type: Sequelize.STRING(16),
    allowNull: false,
    comment: '用户名'
  },
  password: {
    type: Sequelize.STRING(64),
    allowNull: false,
    comment: '密码'
  },
  avatar: {
    type: Sequelize.STRING,
    allowNull: true,
    comment: '头像',
    defaultValue:'http://47.119.158.229:3000/dog.png'
  },
  nickname: {
    type: Sequelize.STRING(16),
    allowNull: true,
    comment: '昵称'
  },
  truename: {
    type: Sequelize.STRING(11),
    allowNull: true,
    comment: '真实姓名'
  },
  phone: {
    type: Sequelize.INTEGER(11),
    allowNull: true,
    comment: '电话'
  },
  brief: {
    type: Sequelize.STRING(32),
    allowNull: true,
    comment: '个性签名'
  },
  gender: {
    type: Sequelize.INTEGER(1),
    allowNull: true,
    comment: '性别'
  },
  email: {
    type: Sequelize.STRING(32),
    allowNull: true,
    comment: '邮箱'
  },
  address: {
    type: Sequelize.STRING(32),
    allowNull: true,
    comment: '地址'
  },
  login_at: {
    type: Sequelize.STRING(32),
    allowNull: true,
    comment: '登录时间'
  },
  token: {
    type: Sequelize.STRING(32),
    allowNull: true,
    comment: 'token'
  },
}, {
  sequelize,
  modelName: 'user',
  tableName: 'user'
})

// user.sync({
//   force: false
// })

module.exports = user