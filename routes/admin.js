const router = require('koa-router')()
const admin = require('../controller/admin')
const {Auth} = require('../middlewares/check')

router.get('/test',admin.test)
router.post('/login',admin.login)
router.post('/create',admin.create)
router.get('/getAllAdmin',admin.getAllAdmin)

module.exports = router