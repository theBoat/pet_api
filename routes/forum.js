const router = require('koa-router')()
const forum = require('../controller/forum')
const {Auth} = require('../middlewares/check')

router.post('/publishComment',forum.publishComment)
router.post('/publish',forum.publish)
router.get('/getAllPost',forum.getAllPost)
router.get('/getOnePost',forum.getOnePost)
router.get('/getPostComment',forum.getPostComment)
router.get('/getUserPost',forum.getUserPost)

module.exports = router