const router = require('koa-router')()
const {Auth,Cors} = require('../middlewares/check')

const test = require('./test')
const user = require('./user')
const pet = require('./pet')
const admin = require('./admin')
const role = require('./role')
const permission = require('./permission')
const shop = require('./shop')
const order = require('./order')
const forum = require('./forum')

router.use('',test.routes())
router.use('/user',user.routes())
router.use('/pet',new Auth().check,pet.routes())
router.use('/admin',admin.routes())
router.use('/role',role.routes())
router.use('/permission',permission.routes())
router.use('/shop',shop.routes())
router.use('/order',order.routes())
router.use('/forum',forum.routes())

module.exports = router