const router = require('koa-router')()
const order = require('../controller/order')
const {Auth} = require('../middlewares/check')

router.post('/create',order.create)
router.post('/updateOrderStatus',order.updateOrderStatus)
router.get('/getMyOrder',order.getMyOrder)
router.get('/getOrderDetail',order.getOrderDetail)
router.get('/getPetOrder',order.getPetOrder)
router.get('/getAllOrder',order.getAllOrder)
router.get('/searchOrder',order.searchOrder)
router.get('/getAdminOrder',order.getAdminOrder)
router.post('/evaluation',order.evaluation)

module.exports = router