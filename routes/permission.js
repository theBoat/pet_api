const router = require('koa-router')()
const permission = require('../controller/permission')

router.post('/create',permission.create)
router.post('/edit',permission.edit)
router.get('/getAllRules',permission.getAllRules)

module.exports = router