const router = require('koa-router')()
const pet = require('../controller/pet')

router.post('/createPet',pet.createPet)
router.get('/deleltePet',pet.deleltePet)
router.get('/getUserPets',pet.getUserPets)
router.get('/getPetInfo',pet.getPetInfo)
router.post('/updatePet',pet.updatePet)

module.exports = router