const router = require('koa-router')()
const role = require('../controller/role')

router.post('/create',role.create)
router.post('/edit',role.edit)
router.get('/getAllRole',role.getAllRole)
router.get('/getRoleRules',role.getRoleRules)

router.post('/setRules',role.setRules)  // x
router.get('/getRules',role.getRules)  // x 
router.post('/setRole',role.setRole)  // x

module.exports = router