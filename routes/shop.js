const router = require('koa-router')()
const shop = require('../controller/shop')

router.post('/update',shop.update)
router.get('/getOneShop',shop.getOneShop)
router.post('/createService',shop.createService)
router.get('/getService',shop.getService)
router.post('/updateService',shop.updateService)
router.post('/delService',shop.delService)
router.get('/getNearbyShop',shop.getNearbyShop)
router.get('/getShopInfo',shop.getShopInfo)
router.get('/searchService',shop.searchService)
router.get('/searchShop',shop.searchShop)
router.get('/getServiceById',shop.getServiceById)
router.get('/getAllShop',shop.getAllShop)
router.get('/getShopUser',shop.getShopUser)
router.get('/getShopComment',shop.getShopComment)

module.exports = router