const router = require('koa-router')()
const user = require('../controller/user')
const chat = require('../controller/chat')
const {Auth} = require('../middlewares/check')

router.post('/register',user.register)
router.post('/login',user.login)
router.post('/ediUserInfo',new Auth().check,user.ediUserInfo)
router.get('/getUserInfo',new Auth().check,user.getUserInfo)
router.get('/getAllUser',new Auth().check,user.getAllUser)
router.get('/searchUser',new Auth().check,user.searchUser)
router.get('/getUserChat',new Auth().check,chat.getUserChat)

module.exports = router